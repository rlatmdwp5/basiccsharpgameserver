﻿using ServerCore;
using System.Net;
using System.Threading;

namespace GameServer
{
    class Program
    {
        static Listener _listener = new Listener();
        public static GameRoom Room = new GameRoom();

        static void FlushRoom()
        {
            Room.Push(() =>
            {
                Room.Flush();
            });

            JobTimer.Instance.Push(FlushRoom, 250);
        }

        static void Main(string[] args)
        {
            string hostName = Dns.GetHostName();
            IPHostEntry ipHostEntry = Dns.GetHostEntry(hostName);
            IPAddress ipAddress = ipHostEntry.AddressList[0];
            IPEndPoint endPoint = new IPEndPoint(ipAddress, 7777);

            _listener.sessionFactory += () => { return SessionManager.Instance.Generate(); };
            _listener.Init(endPoint);

            JobTimer.Instance.Push(FlushRoom);
            while(true)
            {
                JobTimer.Instance.Flush();
            }
        }
    }
}
