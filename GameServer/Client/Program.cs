﻿using ServerCore;
using System;
using System.Net;
using System.Threading;

namespace Client
{

    class Program
    {
        static void Main(string[] args)
        {
            string hostName = Dns.GetHostName(); //dns에서 호스트 이름을 구함
            IPHostEntry iPHostEntry = Dns.GetHostEntry(hostName); //dns에서 호스트 이름을 통해서 iphostentry를 구함.
            IPAddress ipAddress = iPHostEntry.AddressList[0]; //iphostentry에서 ip주소를 구할수 있다. 하나의 호스트에 ip는 여러개 일 수있다.
            IPEndPoint endPoint = new IPEndPoint(ipAddress, 7777); //ip주소와 포트번호를 이용해서 종단점을 만든다.

            Connector connector = new Connector(); // 커넥터를 통해 서버에 연결한다.

            //서버와 통신중에 데이터 관리하게될 세션이 필요하다.
            //클라이언트가 사용하게 될 세션을 생성할 수 있도록 func타입으로 지정한다.
            connector.sessionFactory += () => { return SessionManager.Instance.Generate(); };

            //종단점을 통해서 실제로 연결한다.
            connector.Connect(endPoint, 100);

            //메인 스레드가 끝나지 않도록
            while (true)
            {
                try
                {
                    SessionManager.Instance.SendForEach();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

                Thread.Sleep(250);
            }
        }
    }
}
