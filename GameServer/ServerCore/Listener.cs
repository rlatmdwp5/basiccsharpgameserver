﻿using System;
using System.Net;
using System.Net.Sockets;

namespace ServerCore
{
    public class Listener
    {
        public Func<Session> sessionFactory;

        private Socket _listenSocket;

        public void Init(IPEndPoint endPoint, int register = 10, int backlog = 100)
        {
            _listenSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _listenSocket.Bind(endPoint);
            _listenSocket.Listen(backlog);

            for(int i=0; i<register; i++)
            {
                SocketAsyncEventArgs socketAsyncEvent = new SocketAsyncEventArgs();
                socketAsyncEvent.Completed += new System.EventHandler<SocketAsyncEventArgs>(OnAcceptCompleted);
                RegisterAccept(socketAsyncEvent);
            }
        }

        public void RegisterAccept(SocketAsyncEventArgs socketAsyncEvent)
        {
            // 소켓을 초기화 하지 않고 사용하게 되면 SocketAsyncEventArgs를 계속 재사용하고 있기 때문에 크래쉬 날 여지가 있다.
            socketAsyncEvent.AcceptSocket = null;

            bool pending = _listenSocket.AcceptAsync(socketAsyncEvent);

            if(pending == false)
            {
                //pending이 계속 false일 경우 RegisterAccept() 와 OnAcceptCompleted는 순환 구조를 갖는다.
                //따라서 이론적으론 스택오버플로우가 발생할 수 있다.
                //하지만 현실적으로 Socekt.Listen backlog도 설정 되어 있고, 수많은 유저가 작정하고 하지 않는 이상 스택오버플로우가 발생하긴 쉽지 않다.
                //그래도 불안하면 처리가 필요할듯.
                OnAcceptCompleted(null, socketAsyncEvent);
            }
        }

        public Socket Accept()
        {
            return null;
        }

        #region callback


        //Red Zone
        private void OnAcceptCompleted(object sender, SocketAsyncEventArgs socketAsyncEvent)
        {
            if(socketAsyncEvent.SocketError == SocketError.Success)
            {
                Session session = sessionFactory.Invoke();
                session.Start(socketAsyncEvent.AcceptSocket);

                // fixme
                // OnConnected가 불리기전에 클라이언트가 연결을 끊게 되면 OnConnected가 호출 안될 수 있다.
                session.OnConnected(socketAsyncEvent.AcceptSocket.RemoteEndPoint);
            }
            else
            {
                Console.WriteLine(socketAsyncEvent.SocketError);
            }

            RegisterAccept(socketAsyncEvent);
        }

        #endregion
    }
}
